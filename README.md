# AngularPlayground

Progetto di esempio usato per i corsi di Angular.

## Requisiti

Per poter lavorare lavorare correttamente sul progetto sono necessari:
- [NodeJS e NPM](https://nodejs.org/it/)
- [Angular CLI](https://angular.io/cli)

Scaricare ed installare <b>NodeJS</b> (LTS), per <b>Angular CLI</b> seguire le istruzioni che seguono.

### Angular CLI

Dopo aver installato <b>NodeJS</b>, aprire un terminale e lanciare il comando `npm install --global @angular/cli`.

## Installazione dipendenze ed avvio

Aprire un terminale nella root di progetto e lanciare il comando `npm install`.
Questo comando permette l'installazione delle dipendenze del progetto segnate all'interno del file `package.json`.

Una volta terminata l'installazione, è possibile avviare l'applicazione lanciando, sempre dalla root di progetto, il comando `npm start` e visitare da browser l'indirizzo `http://localhost:4200`.

## Aiuto e chiarimenti

Per chiarimenti sulla Angular CLI lanciare il comando `ng help` o visitare [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

Per aiuto e chiarimenti generali, è possibile contattare i creatori del progetto e detentori del corso riportati nella sezione <b>[Crediti](#crediti)</b>

## Crediti

#### Andrea Pruccoli
<b>Email:</b> andrea.pruccoli[at]maggioli.it
