import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GitHubCommit, GitHubRepository } from 'src/app/features/models/github';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GitHubService {
  readonly url: string = 'https://api.github.com';

  constructor(private http: HttpClient) { }

  getRepositories(user: string): Observable<GitHubRepository[]> {
    return this.http.get<GitHubRepository[]>(`${this.url}/orgs/${user}/repos`);
  }

  getRepository(user: string, repository: string): Observable<GitHubRepository> {
    return this.http.get<GitHubRepository>(`${this.url}/repos/${user}/${repository}`);
  }

  getCommits(user: string, repository: string): Observable<GitHubCommit[]> {
    return this.http.get<GitHubCommit[]>(`${this.url}/repos/${user}/${repository}/commits`);
  }
}
