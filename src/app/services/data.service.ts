import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Dipendente } from 'src/app/features/models/dipendente';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private fakeData = [
    {
      id: 0,
      nome: 'Andrea',
      sede: 'Santarcangelo',
    }, {
      id: 1,
      nome: 'Gioele',
      sede: 'Santarcangelo',
    }, {
      id: 2,
      nome: 'Romualdo',
      sede: 'Bruxelles',
    }, {
      id: 3,
      nome: 'Alexandros',
      sede: 'Atene',
    }
  ];

  message = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';

  constructor() { }

  getDipendenti(): Dipendente[] {
    return this.fakeData;
  }

  getDipendentiAsync(empty: boolean = false): Observable<Dipendente[]> {
    const toReturn = empty ? null : this.fakeData;

    return of(toReturn).pipe(delay(3000));
  }

  getMessageAsync(): Observable<string> {
    return of(this.message).pipe(delay(3000));
  }
}
