import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Supereroe } from 'src/app/features/models/supereroe';

@Injectable({
  providedIn: 'root'
})
export class SupereroiService {

  private url = 'http://localhost:8000';

  constructor(private http: HttpClient) { }

  create(supereroe: Supereroe) {
    this.http.put<Supereroe>(this.url, supereroe);
  }
}
