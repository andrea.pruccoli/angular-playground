import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { ComponenteXModule } from 'src/app/features/components/componente-x/componente-x.module';

import { AuthGuard } from 'src/app/core/auth/auth-guard.service';
import { ComponenteEComponent } from 'src/app/features/components/componente-x/componente-d/componente-e/componente-e.component';
import { ComponenteFComponent } from 'src/app/features/components/componente-x/componente-d/componente-f/componente-f.component';

import { ComponentiComponent } from './componenti.component';

const route: Routes = [
  {
    path: '',
    component: ComponentiComponent,
    children: [
      { path: 'componente-e', component: ComponenteEComponent },
      { path: 'componente-f', component: ComponenteFComponent },
    ],
    // Questo ci assicura che i figli sono navigabili
    // solo se il metodo canActivateChild di AuthGuard
    // ritorna true
    canActivateChild: [AuthGuard],
  }
];

@NgModule({
  declarations: [
    ComponentiComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(route),
    ComponenteXModule,
  ]
})
export class ComponentiModule { }
