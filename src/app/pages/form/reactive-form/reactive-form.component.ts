import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';

import { forbiddenNameValidator } from 'src/app/features/validators/forbidden-name';
import { Supereroe } from 'src/app/features/models/supereroe';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {

  poteri = ['Superforza', 'Volo', 'Invisibilità', 'Sensi di ragno'];

  model: Supereroe;
  // new Supereroe('Bruce Banner', this.poteri[0], 'Hulk');
  // new Supereroe('Peter Parker', this.poteri[3], 'Spiderman');

  supereroeForm = this.fb.group({
    identita: this.fb.group(
      {
        nome: ['', [
          Validators.required,
          Validators.minLength(2),
        ]],
        cognome: ['', [
          Validators.minLength(2),
        ]],
      },
      {
        validator: forbiddenNameValidator(/Giorgio Vanni/i, (control) => `${control.value.nome} ${control.value.cognome}`),
      }
    ),
    soprannome: '',
    potere: ['', Validators.required],
  });

  submitted = false;

  constructor(
    private fb: UntypedFormBuilder,
  ) { }

  ngOnInit() {
  }

  get identita() { return this.supereroeForm.get('identita'); }

  get nome() { return this.supereroeForm.get('identita.nome'); }

  get cognome() { return this.supereroeForm.get('identita.cognome'); }

  get soprannome() { return this.supereroeForm.get('soprannome'); }

  get potere() { return this.supereroeForm.get('potere'); }

  onSubmit() {
    this.submitted = true;

    // (Shallow) Copy of form data
    const result: Supereroe = Object.assign({}, this.supereroeForm.value);
    result.identita = Object.assign({}, result.identita);

    this.model = result;
    console.log(this.model);
  }

  resetForm() {
    this.supereroeForm.reset();
  }
}
