import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';

import { DirectivesComponent } from './directives.component';
import { SharedDirectivesModule } from 'src/app/features/directives/shared-directives.module';

const route: Routes = [
  {
    path: '',
    component: DirectivesComponent,
  }
];

@NgModule({
  declarations: [
    DirectivesComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(route),
    SharedDirectivesModule,
  ]
})
export class DirectivesModule { }
