import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.scss']
})
export class DirectivesComponent implements OnInit {
  backgroundColor: string;
  backgroundColorChangedTimes: number;
  names: string[];

  constructor() {
    this.backgroundColorChangedTimes = 0;
    this.names = [];
  }

  ngOnInit() {
    this.names.push('Andrea');
    this.names.push('Gioele');
    this.names.push('Nicola');
    this.names.push('Romualdo');
  }

  onBackgroundColorClick(color: string) {
    this.backgroundColor = color;
    this.backgroundColorChangedTimes += 1;
  }

}
