import { Component, OnInit, ViewChild } from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";

@Component({
  selector: "app-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.scss"],
})
export class TableComponent implements OnInit {
  @ViewChild('grid') grid: AgGridAngular;

  columnDefs = [
    { headerName: "Nome", field: "nome", sortable: true, filter: true },
    {
      headerName: "Soprannome",
      field: "soprannome",
      sortable: true,
      filter: true,
    },
    { headerName: "Potere", field: "potere", sortable: true, filter: true },
    { headerName: "Età", field: "eta", sortable: true, filter: true },
  ];

  rowData = [
    { nome: "Bruce Banner", soprannome: "Hulk", potere: "superforza", eta: 38 },
    {
      nome: "Peter Parker",
      soprannome: "Spiderman",
      potere: "sensi di ragno",
      eta: 22,
    },
    { nome: "Bruce Wayne", soprannome: "Batman", eta: 30 },
  ];

  constructor() {}

  ngOnInit() {}

  getSelectedRows() {
    const selectedNodes = this.grid.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
    const text = selectedData.map(node => `${node.nome} ${node.soprannome}`).join(', ');
    console.log('Righe selezionate:', text);
  }
}
