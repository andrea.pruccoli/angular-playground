import { NgModule } from "@angular/core";
import { AgGridModule } from "ag-grid-angular";
import { SharedModule } from "src/app/shared/shared.module";
import { TableComponent } from "./table/table.component";
import { TablesRoutingModule } from "./tables-routing.module";

@NgModule({
  imports: [SharedModule, TablesRoutingModule, AgGridModule],
  declarations: [TableComponent],
})
export class TablesModule {}
