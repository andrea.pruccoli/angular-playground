import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Dipendente } from 'src/app/features/models/dipendente';
import { DataService } from 'src/app/services/data.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ResolveDataResolver implements Resolve<Dipendente[]> {

  constructor(private dataService: DataService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Dipendente[]> {
    // return this.dataService.getDipendentiAsync(true)
    //   .pipe(map(dipendenti => {
    //     if (dipendenti) {
    //       return dipendenti;
    //     } else {
    //       this.router.navigate(['/']);
    //       console.log('Nessun dato, ridirigo alla home.');
    //       return null;
    //     }
    //   }));

    return this.dataService.getDipendentiAsync()
      .pipe(map(dipendenti => {
        if (dipendenti) {
          return dipendenti;
        } else {
          this.router.navigate(['/']);
          return null;
        }
      }));
  }
}
