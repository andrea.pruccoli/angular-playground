import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { ResolveComponent } from './resolve.component';
import { ResolveDataResolver } from './resolve-data-resolver.service';
import { SharedModule } from 'src/app/shared/shared.module';

const route: Routes = [
  {
    path: '',
    component: ResolveComponent,
    resolve: {
      dipendenti: ResolveDataResolver,
    }
  }
];

@NgModule({
  declarations: [
    ResolveComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(route),
  ]
})
export class ResolveModule { }
