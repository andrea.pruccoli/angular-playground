import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.scss']
})
export class PipesComponent implements OnInit {
  today: Date;
  balance: number;
  text: string;
  // Notazione col $ alla fine per indicare a noi che è un Observable
  message$: Observable<string>;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.today = new Date();
    this.balance = 154879.36;
    this.text = 'Lorem ipsum dolor sit amet';

    this.message$ = this.dataService.getMessageAsync();
  }

}
