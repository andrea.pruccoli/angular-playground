import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Supereroe } from '../../models/supereroe';

@Component({
  selector: 'app-dati-supereroe',
  templateUrl: './dati-supereroe.component.html',
  styleUrls: ['./dati-supereroe.component.scss']
})
export class DatiSupereroeComponent {

  @Input() model: Supereroe;
  @Output() edit = new EventEmitter<Supereroe>();

  constructor() { }

  onEdit() {
    this.edit.emit(this.model);
  }

}
