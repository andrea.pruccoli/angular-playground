import { NgModule } from '@angular/core';

import { DatiSupereroeComponent } from './dati-supereroe.component';

@NgModule({
  declarations: [
    DatiSupereroeComponent,
  ],
  imports: [],
  exports: [
    DatiSupereroeComponent,
  ],
})
export class DatiSupereroeModule { }
