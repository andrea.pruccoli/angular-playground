import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';

import { ComponenteAComponent } from './componente-a/componente-a.component';
import { ComponenteBComponent } from './componente-b/componente-b.component';
import { ComponenteCComponent } from './componente-c/componente-c.component';
import { ComponenteDComponent } from './componente-d/componente-d.component';
import { ComponenteEComponent } from './componente-d/componente-e/componente-e.component';
import { ComponenteFComponent } from './componente-d/componente-f/componente-f.component';

@NgModule({
  declarations: [
    ComponenteAComponent,
    ComponenteBComponent,
    ComponenteCComponent,
    ComponenteDComponent,
    ComponenteEComponent,
    ComponenteFComponent,
  ],
  imports: [
    SharedModule,
    RouterModule,
  ],
  exports: [
    ComponenteAComponent,
    ComponenteBComponent,
    ComponenteCComponent,
    ComponenteDComponent,
    ComponenteEComponent,
    ComponenteFComponent,
  ],
})
export class ComponenteXModule { }
