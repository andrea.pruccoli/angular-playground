export class Supereroe {

  constructor(
    public identita: Identita,
    public potere: string,
    public soprannome?: string,
    public id?: number,
  ) {  }

}

export class Identita {
  constructor(
    public nome: string,
    public cognome?: string,
  ) { }
}
