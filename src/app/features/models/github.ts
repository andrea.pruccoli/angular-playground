export class GitHubRepository {
  constructor(
    public id: number,
    public name: string,
    public full_name: string,
    public owner: GitHubUser,
    public stars: number,
    public description: string,
    public forks_count: number,
    public watchers_count: number,
    public stargazers_count: number,
  ) {}
}

export class GitHubCommit {
  constructor(
    public sha: number,
    public commit: { message: string },
    public sede: string,
    public author: GitHubUser,
    public committer: GitHubUser,
  ) {}
}

export class GitHubUser {
  constructor(
    public login: string,
    public avatar_url: string,
    public url: string,
    public type: string,
  ) {}
}
