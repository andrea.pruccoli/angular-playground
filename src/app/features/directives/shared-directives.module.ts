import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightOnClickDirective } from './highlight-on-click.directive';
import { UnlessDirective } from './unless.directive';

@NgModule({
  declarations: [
    HighlightOnClickDirective,
    UnlessDirective,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HighlightOnClickDirective,
    UnlessDirective,
  ]
})
export class SharedDirectivesModule { }
