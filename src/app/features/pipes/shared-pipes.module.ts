import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StringLengthPipe } from './string-length.pipe';

@NgModule({
  declarations: [
    StringLengthPipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    StringLengthPipe,
  ],
})
export class SharedPipesModule { }
