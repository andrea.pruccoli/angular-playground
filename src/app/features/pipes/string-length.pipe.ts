import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringLength'
})
export class StringLengthPipe implements PipeTransform {

  // Il primo parametro è l'oggetto a cui è applicata la pipe (e quello sul quale si lavorerà),
  // mentre tutti i successivi parametri saranno le varie opzioni
  transform(value: string, trimWhitespaces: boolean = false): string {
    const toPipe = trimWhitespaces ? value.replace(/\s/g, '') : value;

    return `${toPipe} (${toPipe.length})`;
  }

}
