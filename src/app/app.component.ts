import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { NgxProgressService } from '@kken94/ngx-progress';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Angular Playground';

  constructor(
    private router: Router,
    private ngxProgressBar: NgxProgressService,
  ) { }

  ngOnInit() {
    this._registerNavigationEvents();
  }

  private _registerNavigationEvents() {
    this.router.events.subscribe(
      (event: Event) => {
        switch (true) {
          case event instanceof NavigationStart:
            this.ngxProgressBar.start();
            break;
          case event instanceof NavigationEnd:
          case event instanceof NavigationCancel:
          case event instanceof NavigationError:
            this.ngxProgressBar.end();
            break;
          default:
            break;
        }
      }
    );
  }
}
