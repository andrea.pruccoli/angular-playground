import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { CanActivate, CanActivateChild, CanLoad } from "@angular/router";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { AuthService } from "./auth.service";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) {}

  /**
   * route e state vengono passati da Angular
   * nel momento in cui viene effettuata la navigazione tra le pagine.
   *
   * La possibilità di ritornare Observable|Promise|boolean, permette
   * l'operabilità in modo asincrono/sincrono.
   */
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    // Observable
    return this.authService.isAuthenticated().pipe(
      map((authenticated: boolean) => {
        if (authenticated) {
          return true;
        } else {
          this.snackBar.open(
            "Utente non autenticato. Impossibile accedere alla pagina.",
            null,
            {
              duration: 5000,
              verticalPosition: "top",
            }
          );
          console.log(
            "Utente non autenticato, impossibile effettuare la navigazione."
          );
          // Per portarlo in uno state nostro
          // this.router.navigate(['/']);

          // Per restare nello state in cui si trova
          return false;
        }
      })
    );
  }

  canActivateChild(): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate();
  }

  /**
   * route viene passato da Angular nel momenti in cui si tenta
   * di navigare il ruote a cui canLoad fa riferimento
   */
  canLoad(): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate();
  }
}
