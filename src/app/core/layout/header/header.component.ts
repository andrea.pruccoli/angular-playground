import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  navbarOpen = false;
  loggedIn = false;

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  onLogin() {
    this.authService.login().subscribe(
      (value: boolean) => {
        console.log('Login effettuato');
        this.loggedIn = value;
      }
    );
  }

  onLogout() {
    this.authService.logout().subscribe(
      (value: boolean) => {
        console.log('Logout effettuato');
        this.loggedIn = value;
      }
    );
  }
}
