import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from 'src/app/core/auth/auth-guard.service';

const routes: Routes = [
  { path: '', loadChildren: () => import('src/app/pages/home/home.module').then(m => m.HomeModule) },
  {
    path: 'components',
    loadChildren: () => import('src/app/pages/componenti/componenti.module').then(m => m.ComponentiModule),
  },
  {
    path: 'routing-parametrized',
    loadChildren: () => import('src/app/pages/routing/routing.module').then(m => m.RoutingModule),
  },
  { path: 'directives', loadChildren: () => import('src/app/pages/directives/directives.module').then(m => m.DirectivesModule) },
  { path: 'service', loadChildren: () => import('src/app/pages/service/service.module').then(m => m.ServiceModule) },
  { path: 'pipes', loadChildren: () => import('src/app/pages/pipes/pipes.module').then(m => m.PipesModule) },
  { path: 'resolve', loadChildren: () => import('src/app/pages/resolve/resolve.module').then(m => m.ResolveModule) },
  { path: 'form', loadChildren: () => import('src/app/pages/form/form.module').then(m => m.FormModule) },
  {
    path: 'tables',
    loadChildren: () => import('src/app/pages/tables/tables.module').then(m => m.TablesModule),
  },
  { path: 'admin', loadChildren: () => import('src/app/pages/amministrazione/amministrazione.module').then(m => m.AmministrazioneModule), canLoad: [AuthGuard] },
  { path: 'not-found', loadChildren: () => import('src/app/pages/page-not-found/page-not-found.module').then(m => m.PageNotFoundModule) },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes, {})],
})
export class AppRoutingModule { }
